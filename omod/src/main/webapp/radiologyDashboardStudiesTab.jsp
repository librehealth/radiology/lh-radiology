<%@ include file="/WEB-INF/view/module/radiology/dashboardHeader.jsp"%>
<openmrs:htmlInclude file="/moduleResources/radiology/vendor/jquery-date-range-picker/daterangepicker.min.css" />
<openmrs:htmlInclude file="/moduleResources/radiology/vendor/jquery-date-range-picker/jquery.daterangepicker.min.js" />
<script type="text/javascript">
  var $j = jQuery.noConflict();

  $j(document)
          .ready(
                  function() {
                	    var accessionNumber = $j('#studiesTabAccessionNumberFilter');
                      var patientUuid = $j('#studiesTabPatientFilter');
                      var modality = $j('#studiesTabModalityFilter');
                      var fromEffectiveStartDate = $j('#studiesTabFromEffectiveStartDateFilter');
                      var toEffectiveStartDate = $j('#studiesTabToEffectiveStartDateFilter');
                      var urgency = $j('#studiesTabUrgencySelect');
                      var find = $j('#studiesTabFind');
                      var clearResults = $j('a#studiesTabClearFilters');

                      if (typeof (Storage) !== "undefined") {
                        accessionNumber.val(sessionStorage
                                .getItem("accessionNumber"));
                        $j("#studiesTabPatientFilter_selection").val(
                                sessionStorage.getItem("patientName"));
                        patientUuid.val(sessionStorage.getItem("patientUuid"));
                        fromEffectiveStartDate.val(sessionStorage
                                .getItem("fromEffectiveStartDate"));
                        toEffectiveStartDate.val(sessionStorage
                                .getItem("toEffectiveStartDate"));
                        
                        modality.val(sessionStorage.getItem("modality"));
                      }

                      $j("#radiologyStudiesTab").parent().addClass(
                              "ui-tabs-selected ui-state-active");

                      var radiologyStudiesTable = $j('#studiesTabTable')
                              .DataTable(
                                      {
                                        searching: true,
                                      //   "processing": true,
                                      //   "serverSide": true,
                                        "ajax": {
                                          headers: {
                                            Accept: "application/json; charset=utf-8",
                                            "Content-Type": "text/plain; charset=utf-8",
                                          },
                                          cache: true,
                                          dataType: "json",
                                          dataSrc: "",
                                          url: Radiology.getRestRootEndpoint() + "/orthanc/studies",
                                          data: function(data) {
                                            return {
                                              // startIndex: data.start,
                                              // // limit: data.length,
                                              // v: "full",
                                              // accessionNumber: accessionNumber.val(),
                                              // patient: patientUuid.val(),
                                              // fromEffectiveStartDate: fromEffectiveStartDate
                                              //         .val() === ""
                                              //         ? ""
                                              //         : moment(
                                              //                 fromEffectiveStartDate
                                              //                         .val(),
                                              //                 "L LT")
                                              //                 .format(
                                              //                         "YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                              // toEffectiveStartDate: toEffectiveStartDate
                                              //         .val() === ""
                                              //         ? ""
                                              //         : moment(
                                              //                 toEffectiveStartDate
                                              //                         .val(),
                                              //                 "L LT")
                                              //                 .format(
                                              //                         "YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                              // modality: modality.val(),
                                              // totalCount: true,
                                            };
                                          },
                                          "dataFilter": function(data) {
                                            var json = $j.parseJSON(data);
                                          //   json.recordsTotal = json.totalCount || 0;
                                          //   json.recordsFiltered = json.totalCount || 0;
                                          //   json.data = json.results;
                                            return JSON.stringify(json);
                                          },
                                          error: function(jqXHR, textStatus,
                                                  errorThrown) {
                                            
                                          	Radiology.showAlertDialog(
                                                            '<spring:message code="radiology.rest.error.dialog.title"/>',
                                                            '<spring:message code="radiology.rest.error.dialog.message.line1"/><br />'
                                                                    + '<spring:message code="radiology.rest.error.dialog.message.line2"/>',
                                                            '<spring:message code="radiology.rest.error.dialog.button.ok"/>');
                                            
                                            
                                            
                                            console.error("A rest error occured - "
                                                            + textStatus
                                                            + ":\n"
                                                            + errorThrown, jqXHR);
                                            
                                        	  $j("#studiesTabTable_processing").hide();
                                          }
                                        },
                                        "columns": [
                                            {
                                              "className": "control",
                                              "orderable": false,
                                              "data": null,
                                              "defaultContent": "",
                                              "responsivePriority": 1
                                            },
                                            {
                                              "name": "accessionNumber",
                                              "responsivePriority": 1,
                                              "render": function(data, type,
                                                      full, meta) {
                                                return full.MainDicomTags.AccessionNumber;
                                              }
                                            },
                                            {
                                              "name": "patient",
                                              "render": function(data, type,
                                                      full, meta) {
                                                return full.PatientMainDicomTags.PatientName;
                                              }
                                            },
                                            {
                                              "name": "modality",
                                              "render": function(data, type,
                                                      full, meta) {
                                               	return full.Modality;
                                              }
                                            },
                                            {
                                              "name": "studyDescription",
                                              "render": function(data, type,
                                                      full, meta) {
                                                return full.MainDicomTags.StudyDescription;
                                              }
                                            },
                                            {
                                              "name": "studyDate",
                                              "render": function(data, type,
                                                      full, meta) {
                                                var result = "";
                                                if (full.MainDicomTags.StudyDate) {

                                                  result = moment(
                                                          full.MainDicomTags.StudyDate)
                                                          .format("LLL");
                                                }
                                                
                                                return result;
                                              }
                                            },
                                            {
                                              "name": "action",
                                              "className": "dt-center",
                                              "responsivePriority": 1,
                                              "render": function(data, type,
                                                      full, meta) {
                                                return '<a href="#">Claim Study</a>';
                                              }
                                            }
                                      ],
                                      });
                      

                      function storeFilters() {
                        if (typeof (Storage) !== "undefined") {
                          sessionStorage.setItem("accessionNumber",
                                  accessionNumber.val());
                          sessionStorage.setItem("patientName", $j(
                                  "#studiesTabPatientFilter_selection").val());
                          sessionStorage
                                  .setItem("patientUuid", patientUuid.val());
                          sessionStorage.setItem("fromEffectiveStartDate",
                                  fromEffectiveStartDate.val());
                          sessionStorage.setItem("toEffectiveStartDate",
                                  toEffectiveStartDate.val());
                          sessionStorage.setItem("modality", modality.val());
                        }
                      }

                      $j("#studiesTabTableFilters input:visible:enabled:first").focus();
                      
                      find.add(accessionNumber).add(
                              "#studiesTabPatientFilter_selection").add(
                              fromEffectiveStartDate).add(toEffectiveStartDate)
                              .add(modality).keypress(function(event) {
                                if (event.which == 13) {
                                  event.preventDefault();
                                  radiologyStudiesTable.ajax.reload();
                                  storeFilters();
                                }
                              });
                              
                      find.click(function() {
                        radiologyStudiesTable.ajax.reload();
                        storeFilters();
                      });

                      clearResults
                              .on(
                                      'mouseup keyup',
                                      function() {
                                        $j(
                                                '#studiesTabTableFilters input, #studiesTabTableFilters select')
                                                .val('');
                                        $j(
                                                '#studiesTabEffectiveStartDateRangePicker')
                                                .data('dateRangePicker').clear();
                                        $j(
                                                "#studiesTabTableFilters input:visible:enabled:first")
                                                .focus();
                                        radiologyStudiesTable.ajax.reload();
                                        storeFilters();
                                      });

                      $j('#studiesTabEffectiveStartDateRangePicker')
                              .dateRangePicker(
                                      {
                                        startOfWeek: "monday",
                                        customTopBar: '<b class="start-day">...</b> - <b class="end-day">...</b><i class="selected-days"> (<span class="selected-days-num">3</span>)</i>',
                                        showShortcuts: true,
                                        shortcuts: {
                                          'prev-days': [3, 5, 7],
                                          'prev': ['week', 'month'],
                                          'next-days': null,
                                          'next': null
                                        },
                                        separator: '-',
                                        format: 'L LT',
                                        time: {
                                          enabled: true
                                        },
                                        defaultTime: moment().startOf('day')
                                                .toDate(),
                                        defaultEndTime: moment().endOf('day')
                                                .toDate(),
                                        getValue: function() {
                                          if (fromEffectiveStartDate.val()
                                                  && toEffectiveStartDate.val())
                                            return fromEffectiveStartDate.val()
                                                    + '-'
                                                    + toEffectiveStartDate.val();
                                          else
                                            return '';
                                        },
                                        setValue: function(s, s1, s2) {
                                          fromEffectiveStartDate.val(s1);
                                          toEffectiveStartDate.val(s2);
                                        }
                                      });
                  });
</script>

<openmrs:hasPrivilege privilege="Get Radiology Studies">
  <div id="radiologyStudies">
    <br> <span class="boxHeader"> <b><spring:message code="radiology.dashboard.tabs.radiologyStudies.boxheader" /></b> <a
      id="studiesTabClearFilters" href="#" style="float: right"> <spring:message
          code="radiology.dashboard.tabs.filters.clearFilters" />
    </a>
    </span>
    <div class="box">
      <table cellspacing="10">
        <tr>
          <form>
            <td id="studiesTabTableFilters"><label>
             <spring:message code="radiology.dashboard.tabs.filters.filterby" /></label> 
             <!-- Accession Number -->
             <input type="text" id="studiesTabAccessionNumberFilter"
              placeholder='<spring:message code="radiology.dashboard.tabs.orders.filters.accessionNumber"/>' /> 
             
             <!-- Patient ID or Name -->
             <radiology:patientField formFieldName="patient" formFieldId="studiesTabPatientFilter" /> 
             
             <!-- Modality -->
             <input type="text" id="studiesTabModalityFilter"
              placeholder='<spring:message code="radiology.dashboard.tabs.radiologyStudies.filters.modality"/>' /> 
             
             <span id="studiesTabEffectiveStartDateRangePicker"> 
                <input type="text"
                id="studiesTabFromEffectiveStartDateFilter"
                placeholder='<spring:message code="radiology.dashboard.tabs.orders.filters.effectiveStartDate.from" />' /> 
                <span>-</span>
                <input type="text" id="studiesTabToEffectiveStartDateFilter"
                placeholder='<spring:message code="radiology.dashboard.tabs.orders.filters.effectiveStartDate.to" />' />
            </span>
            
            </td>
            <td><input id="studiesTabFind" type="button"
              value="<spring:message code="radiology.dashboard.tabs.filters.filter"/>" /></td>
          </form>
        </tr>
      </table>
      <br>
      <div>
        <table id="studiesTabTable" cellspacing="0" width="100%" class="display responsive compact">
          <thead>
            <tr>
              <th></th>
              <th><spring:message code="radiology.datatables.column.order.accessionNumber" /></th>
              <th><spring:message code="radiology.datatables.column.order.patient" /></th>
              <th><spring:message code="radiology.datatables.column.studies.modality" /></th>
              <th><spring:message code="radiology.datatables.column.studies.studyDescription" /></th>
              <th><spring:message code="radiology.datatables.column.studies.studyDate" /></th>
              <th><spring:message code="radiology.datatables.column.action" /></th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</openmrs:hasPrivilege>
</div>
<%@ include file="/WEB-INF/template/footer.jsp"%>
