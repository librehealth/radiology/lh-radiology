/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.orthanc.beans.series;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.openmrs.module.radiology.orthanc.beans.MainDicomTags;

public class OrthancSeries {
	
	@JsonProperty("ExpectedNumberOfInstances")
	private Object expectedNumberOfInstances;
	
	@JsonProperty("ID")
	private String id;
	
	@JsonProperty("Instances")
	private List<String> instances = null;
	
	@JsonProperty("IsStable")
	private Boolean isStable;
	
	@JsonProperty("LastUpdate")
	private String lastUpdate;
	
	@JsonProperty("MainDicomTags")
	private MainDicomTags mainDicomTags;
	
	@JsonProperty("ParentStudy")
	private String parentStudy;
	
	@JsonProperty("Status")
	private String status;
	
	@JsonProperty("Type")
	private String type;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	@JsonProperty("ExpectedNumberOfInstances")
	public Object getExpectedNumberOfInstances() {
		return expectedNumberOfInstances;
	}
	
	@JsonProperty("ExpectedNumberOfInstances")
	public void setExpectedNumberOfInstances(Object expectedNumberOfInstances) {
		this.expectedNumberOfInstances = expectedNumberOfInstances;
	}
	
	@JsonProperty("ID")
	public String getId() {
		return id;
	}
	
	@JsonProperty("ID")
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonProperty("Instances")
	public List<String> getInstances() {
		return instances;
	}
	
	@JsonProperty("Instances")
	public void setInstances(List<String> instances) {
		this.instances = instances;
	}
	
	@JsonProperty("IsStable")
	public Boolean getIsStable() {
		return isStable;
	}
	
	@JsonProperty("IsStable")
	public void setIsStable(Boolean isStable) {
		this.isStable = isStable;
	}
	
	@JsonProperty("LastUpdate")
	public String getLastUpdate() {
		return lastUpdate;
	}
	
	@JsonProperty("LastUpdate")
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	@JsonProperty("MainDicomTags")
	public MainDicomTags getMainDicomTags() {
		return mainDicomTags;
	}
	
	@JsonProperty("MainDicomTags")
	public void setMainDicomTags(MainDicomTags mainDicomTags) {
		this.mainDicomTags = mainDicomTags;
	}
	
	@JsonProperty("ParentStudy")
	public String getParentStudy() {
		return parentStudy;
	}
	
	@JsonProperty("ParentStudy")
	public void setParentStudy(String parentStudy) {
		this.parentStudy = parentStudy;
	}
	
	@JsonProperty("Status")
	public String getStatus() {
		return status;
	}
	
	@JsonProperty("Status")
	public void setStatus(String status) {
		this.status = status;
	}
	
	@JsonProperty("Type")
	public String getType() {
		return type;
	}
	
	@JsonProperty("Type")
	public void setType(String type) {
		this.type = type;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}
	
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
	
}
