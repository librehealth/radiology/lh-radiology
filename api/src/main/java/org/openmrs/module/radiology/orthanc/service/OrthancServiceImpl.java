/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.orthanc.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.openmrs.module.radiology.RadiologyProperties;
import org.openmrs.module.radiology.orthanc.beans.series.OrthancSeries;
import org.openmrs.module.radiology.orthanc.beans.study.OrthancStudy;
import org.openmrs.module.radiology.orthanc.exception.OrthancException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class OrthancServiceImpl implements OrthancService {
	
	private static final String uploadPath = "/tools/create-dicom";
	
	private static final String studies = "/studies";
	
	private static final String series = "/series";
	
	@Autowired
	private RadiologyProperties radiologyProperties;
	
	private static final Logger log = LoggerFactory.getLogger(OrthancServiceImpl.class);
	
	@Override
	public String uploadFileToOrthanc(String identifier, String tags, byte[] bytes) throws OrthancException {
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = null;
		
		try {
			HttpHeaders headers = new HttpHeaders();
			
			// Header for Basic Authentication
			byte creds[] = (radiologyProperties.getOrthancUsername() + ":" + radiologyProperties.getOrthancPassword())
			        .getBytes();
			headers.add("Authorization", "Basic " + Base64.getEncoder().encodeToString(creds));
			headers.add("Content-Type", "application/json");
			
			String url = radiologyProperties.getOrthancURL() + uploadPath;
			
			log.debug("Uploading File to Orthanc Server for Study - " + identifier + ", URL - " + url);
			HttpEntity<String> request = new HttpEntity<String>(generateJSONRequest(identifier, bytes, tags), headers);
			
			response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
		}
		catch (Exception e) {
			log.error(e.getMessage());
			throw new OrthancException(e.getMessage());
		}
		
		return response.getBody();
	}
	
	protected String generateJSONRequest(String identifier, byte[] bytes, String tags) {
		String base64File = Base64.getEncoder().encodeToString(bytes);
		return "{\"Parent\": \"" + identifier + "\", \"Tags\" : " + tags + ", \"Content\" : \"data:application/pdf;base64,"
		        + base64File + "\"}";
		
	}
	
	/**
	 * List the Orthanc identifiers of all the available DICOM studies
	 */
	@Override
	public OrthancStudy[] getStudies(String expand, Integer limit, Integer since) throws OrthancException {
		
		RestTemplate restTemplate = new RestTemplate();
		OrthancStudy[] studies = null;
		
		try {
			HttpHeaders headers = new HttpHeaders();
			
			// Header for Basic Authentication
			byte creds[] = (radiologyProperties.getOrthancUsername() + ":" + radiologyProperties.getOrthancPassword())
			        .getBytes();
			
			headers.add("Authorization", "Basic " + Base64.getEncoder().encodeToString(creds));
			headers.add("Content-Type", "application/json");
			
			String url = buildURLParams(expand, limit, since);
			
			log.info("Fetching Studies from Orthanc - [expand = " + expand + ", limit = " + limit + ", since = " + since
			        + "], URL - " + url + ", Headers - " + headers.toString());
			
			HttpEntity<String> request = new HttpEntity<String>(headers);
			
			if (log.isDebugEnabled()) {
				log.debug("Fetching studies from Orthanc. URL - " + url);
				log.debug("Request Body - " + request.toString());
			}
			
			//			ResponseEntity<OrthancStudy[]> response = restTemplate.exchange(url, HttpMethod.GET, request,
			//			    OrthancStudy[].class);
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
			//			String response = restTemplate.getForObject(url, String.class, request);
			
			if (log.isDebugEnabled()) {
				log.debug("Fetched studies from Orthanc. URL - " + url);
				log.debug("Response Body - " + response.toString());
			}
			
			//			studies = response.getBody();
			
			ObjectMapper mapper = new ObjectMapper();
			studies = mapper.readValue(response.getBody(), OrthancStudy[].class);
			//			studies = mapper.readValue(response, OrthancStudy[].class);
			
			populateModalities(studies, request);
		}
		catch (Exception e) {
			log.error(e.getMessage());
			throw new OrthancException(e.getMessage());
		}
		
		if (log.isDebugEnabled()) {
			log.debug("Populated Modalities in studies from Orthanc");
			log.debug("Studies - " + Arrays.toString(studies));
		}
		
		log.debug("Populated Modalities in studies from Orthanc");
		log.debug("Studies - " + Arrays.toString(studies));
		
		return studies;
	}
	
	protected OrthancStudy[] populateModalities(OrthancStudy[] studies, HttpEntity<String> request) throws JsonParseException, JsonMappingException, IOException {
		
		RestTemplate restTemplate = new RestTemplate();
		
		if (studies != null && studies.length != 0) {
			for (OrthancStudy study : studies) {
				if (study.getSeries() != null && study.getSeries().size() != 0) {
					// Fetch the Modality for the first series
					String seriesID = study.getSeries().get(0);
					// ResponseEntity<OrthancSeries> seriesRes = restTemplate.exchange(
					//     radiologyProperties.getOrthancURL() + series + "/" + seriesID, HttpMethod.GET, request,
					//     OrthancSeries.class);
					ResponseEntity<String> seriesRes = restTemplate.exchange(
					    radiologyProperties.getOrthancURL() + series + "/" + seriesID, HttpMethod.GET, request,
					    String.class);
					
					ObjectMapper mapper = new ObjectMapper();
					OrthancSeries series = mapper.readValue(seriesRes.getBody(), OrthancSeries.class);
					
//					OrthancSeries series = seriesRes.getBody();
					if (series != null) {
						study.setModality(series.getMainDicomTags().getModality());
					}
				}
			}
		}
		
		return studies;
	}
	
	protected String buildURLParams(String expand, Integer limit, Integer since) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(radiologyProperties.getOrthancURL() + studies);
		
		if (expand != null && expand.trim() != "") {
			uriBuilder.queryParam("expand", expand);
		}
		
		//		if (limit != null) {
		//			uriBuilder.queryParam("limit", limit);
		//		}
		//		
		//		if (since != null) {
		//			uriBuilder.queryParam("since", since);
		//		}
		
		return uriBuilder.toUriString();
	}
}
